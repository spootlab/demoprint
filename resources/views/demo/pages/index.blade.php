@extends('demo/layout')

@section('content')
    <div class="starter-template">
        <h2>DemoPrint</h2>
        <hr />
        <form class="form" action="/submit" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            
            <div class="form-group">
                <label for="input" class="sr-only">Input</label>
                <textarea id="input" name="input" class="form-control" placeholder="Input" required autofocus></textarea>
            </div>
            <button class="btn btn-primary pull-right" type="submit">Submit</button>
        </form>
    </div>
@endsection

@section('styles')
    <style>
    body {
        padding-top: 50px;
    }
    .starter-template {
        padding: 40px 15px;
        text-align: center;
    }
    </style>
@endsection

@section('scripts')

@endsection
