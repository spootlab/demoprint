@extends('demo/layout')

@section('content')
    <div class="starter-template">
        <div class="row">
            <div class="col-md-6">
                <h4 class="text-left">RECEBIDOS</h4>
                <hr />

                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Pedido</th>
                            <th>Cliente</th>
                            <th>Data</th>
                            <th>Valor</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data['received'] as $item)
                            <tr>
                                <td class="text-left">{{ $item['reference'] }}</td>
                                <td class="text-left">{{ $item['client'] }}</td>
                                <td class="text-left">{{ $item['date'] }}</td>
                                <td class="text-left">R$ {{ $item['amount'] }}</td>
                                <td>
                                    <button data-reference="{{ $item['reference'] }}" data-info="{{json_encode( $item )}}" class="btn btn-info printButton"><i class="glyphicon glyphicon-print"></i></button>
                                    <button data-reference="{{ $item['reference'] }}" data-info="{{json_encode( $item )}}" class="btn btn-danger showButton"><i class="glyphicon glyphicon-eye-open"></i></button>
                                    <button class="btn btn-success"><i class="glyphicon glyphicon-ok"></i></button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="col-md-6">
                <h4 class="text-left">EM PRODUÇÃO</h4>
                <hr />

                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Pedido</th>
                            <th>Cliente</th>
                            <th>Data</th>
                            <th>Valor</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data['inProduction'] as $item)
                            <tr>
                                <td class="text-left">{{ $item['reference'] }}</td>
                                <td class="text-left">{{ $item['client'] }}</td>
                                <td class="text-left">{{ $item['date'] }}</td>
                                <td class="text-left">R$ {{ $item['amount'] }}</td>
                                <td>
                                    <button data-reference="{{ $item['reference'] }}" data-info="{{json_encode( $item )}}" class="btn btn-info printButton"><i class="glyphicon glyphicon-print"></i></button>
                                    <button data-reference="{{ $item['reference'] }}" data-info="{{json_encode( $item )}}" class="btn btn-danger showButton"><i class="glyphicon glyphicon-eye-open"></i></button>
                                    <button class="btn btn-success"><i class="glyphicon glyphicon-ok"></i></button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="col-md-6">
                <h4 class="text-left">SAIU PARA ENTREGA</h4>
                <hr />

                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Pedido</th>
                            <th>Cliente</th>
                            <th>Data</th>
                            <th>Valor</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data['inDelivery'] as $item)
                            <tr>
                                <td class="text-left">{{ $item['reference'] }}</td>
                                <td class="text-left">{{ $item['client'] }}</td>
                                <td class="text-left">{{ $item['date'] }}</td>
                                <td class="text-left">R$ {{ $item['amount'] }}</td>
                                <td>
                                    <button data-reference="{{ $item['reference'] }}" data-info="{{json_encode( $item )}}" class="btn btn-info printButton"><i class="glyphicon glyphicon-print"></i></button>
                                    <button data-reference="{{ $item['reference'] }}" data-info="{{json_encode( $item )}}" class="btn btn-danger showButton"><i class="glyphicon glyphicon-eye-open"></i></button>
                                    <button class="btn btn-success"><i class="glyphicon glyphicon-ok"></i></button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
            <div class="col-md-6">
                <h4 class="text-left">ENTREGUES</h4>
                <hr />

                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Pedido</th>
                            <th>Cliente</th>
                            <th>Data</th>
                            <th>Valor</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data['delivered'] as $item)
                            <tr>
                                <td class="text-left">{{ $item['reference'] }}</td>
                                <td class="text-left">{{ $item['client'] }}</td>
                                <td class="text-left">{{ $item['date'] }}</td>
                                <td class="text-left">R$ {{ $item['amount'] }}</td>
                                <td>
                                    <button data-reference="{{ $item['reference'] }}" data-info="{{json_encode( $item )}}" class="btn btn-info printButton"><i class="glyphicon glyphicon-print"></i></button>
                                    <button data-reference="{{ $item['reference'] }}" data-info="{{json_encode( $item )}}" class="btn btn-danger showButton"><i class="glyphicon glyphicon-eye-open"></i></button>
                                    <button class="btn btn-success"><i class="glyphicon glyphicon-ok"></i></button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <input type="text" id="printerName" name="input" class="form-control" value="PDF Printer: Frederico Lima's Mac" />
            </div>
        </div>
    </div>



    <div id="printModal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 id="modalTitle" class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <div id="modalContent"></div>
                </div>
                <div class="modal-footer">
                    <button id="modalPrintButton" data-reference="" data-info="" type="button" class="btn btn-info btn-block printButton">IMPRIMIR PEDIDO</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection

@section('styles')
    <style>
        body {
            padding-top: 50px;
        }
        .starter-template {
            padding: 40px 15px;
            text-align: center;
        }
    </style>
@endsection

@section('scripts')
    <script type="text/javascript" src="js/qz/dependencies/rsvp-3.1.0.min.js"></script>
    <script type="text/javascript" src="js/qz/dependencies/sha-256.min.js"></script>
    <script type="text/javascript" src="js/qz/qz-tray.js"></script>

    <script type="text/javascript">
       /*******************************************************************
        *** HTML ACTIONS
        *******************************************************************/

        $(".printButton").click(function(){
            var info = $(this).data("info");

            printData(info);
        });

        $(".showButton").click(function(){
            var reference = $(this).data("reference");
            var info = $(this).data("info");

            var modalPrintButton = $("#modalPrintButton");

            $("#modalTitle").html("Pedido #" + reference);
            $("#modalContent").html(JSON.stringify(info));

            modalPrintButton.data('reference', reference);
            modalPrintButton.data('info', JSON.stringify(info));

            $('#printModal').modal();
        });

       /*******************************************************************
        *** QZ.IO FUNCTIONS
        *******************************************************************/

        qz.websocket.connect();

        function printData(data){
            var printer = $("#printerName").val();

            if(printer.length > 0){
                var config = qz.configs.create(printer);               // Exact printer name from OS
                //var data = [""];   // Raw commands (ZPL provided)

                qz.print(config, data).then(function() {
                  alert("Sent data to printer");
                });
            }else{
                alert('ERROR: Printer not found!');
            }
        }
    </script>
@endsection
