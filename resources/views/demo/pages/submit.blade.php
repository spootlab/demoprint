@extends('demo/layout')

@section('content')
    <div class="starter-template">
        <h2>DemoPrint</h2>

        <hr />
        <p class="text-left">{!! nl2br($data) !!}</p>
        <hr />

        <div class="form-group">
            <input type="text" id="printerName" class="form-control" placeholder="Printer Name" />
        </div>

        <div>
            <button id="print" class="btn btn-primary pull-right" type="button">Print</button>
            <button id="findPrinters" class="btn btn-info pull-right" type="button">Find Printers</button>
        </div>
    </div>
@endsection

@section('styles')
    <style>
    body {
        padding-top: 50px;
    }
    .starter-template {
        padding: 40px 15px;
        text-align: center;
    }
    </style>
@endsection

@section('scripts')
    <script type="text/javascript" src="js/qz/dependencies/rsvp-3.1.0.min.js"></script>
    <script type="text/javascript" src="js/qz/dependencies/sha-256.min.js"></script>
    <script type="text/javascript" src="js/qz/qz-tray.js"></script>

    <script type="text/javascript">
        $("#findPrinters").click(function(){
            findPrinters();
        });

        $("#print").click(function(){
            printData();
        });

        qz.websocket.connect();

        function printData(){
            var printer = $("#printerName").val();

            if(printer.length > 0){
                var config = qz.configs.create(printer);               // Exact printer name from OS
                var data = ["{{$dataRaw}}"];   // Raw commands (ZPL provided)

                qz.print(config, data).then(function() {
                  alert("Sent data to printer");
                });
            }else{
                alert('ERROR: Printer not found!');
            }
        }

        function findPrinters() {
            qz.printers.find().then(function(data) {
                var list = '';
                for(var i = 0; i < data.length; i++) {
                    list += "&nbsp; " + data[i] + "<br/>";
                }
                alert("<strong>Available printers:</strong><br/>" + list);
            }).catch(function(e) { console.error(e); });
        }
    </script>
@endsection
